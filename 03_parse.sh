#!/bin/bash


SPECDIR=/tmp/spec/
RESULTDIR=${SPECDIR}/result


# HEPSPEC calc taken from original runspec.sh, as it's filled with scary sed expressions :)
# =========================================================================================

# Calculate the result
# First, lets get a list of the runs to look for (001, 002, etc.)
RUNS=""
for n in ${RESULTDIR}/CPU2006.*.log;
do
    RUNS="$RUNS `echo $n | sed 's/^.*CPU2006\.\(\w\+\)\.log/\1/'`"
done

echo ${RUNS}

SUM=0
for n in $RUNS;
do
    partial=0
    count=0
    # This scary-looking sed expression looks in the results files of a single run
    # (both CINT and CFP files) for the stuff between a line containing all =====,
    # and " Est. SPEC". This is the final results table and lists all the partial results.
    # Within that section, look for lines that look like:
    #   410.bwaves      13590       2690       5.05 *
    # and grab the last number, 5.05
    for b in `sed -n -e '/^=\+$/,/^ Est. SPEC/!d; s/[0-9]\{3\}\.\w\+\s\+[0-9]\+\s\+[0-9]\+\s\+\([0-9.]\+\)\s\+\*/\1/p' ${RESULTDIR}/*.$n.*txt 2>/dev/null`;
    do
        partial="$partial + l($b)"
        count=$(($count + 1))
    done
    if [[ $partial != 0 ]]; # "if the above sed read something..."
    then
        # Calculate the geometric average of all the benchmark results for that run (ie. core)
        # The geometric average of three numbers is: (x * y * z)**1/3
        # or, in order to process this with bc: exp( ( ln(x) + ln(y) + ln(z) ) / 3 )
        SUM="$SUM + `echo "scale=8; e(($partial) / $count)" | bc -l`"
    fi
done
# Add up all the geometric averages and round to the second decimal
SUM=`echo "scale=2; ($SUM)/1" | bc`
echo "Final result: $SUM"
