#!/bin/bash

# Load Global Variables from file
. 00_variables.sh


# Check to make sure HEPSPEC has been installed where we think it should be.
if [ ! -d ${INSTALL} ]; then
	echo "Please ensure HEPSPEC is installed before you attempt to run this script."
	echo "Please ensure that INSTALL is correctly set in the 00_variables.sh file."
	exit 1
fi

# Change to the install location.
cd ${INSTALL}
echo "Working directory: ${PWD}"

# Set up HEPSPEC environment.
. shrc

# Get CPU count
COUNT=`grep -c "^processor" /proc/cpuinfo`
echo "Detected ${COUNT} CPU's"

if [ ${COUNT} -le 0 ]; then
	echo "ERROR: couldn't detect any CPU's, something has gone wrong"
fi

# Start the HEPSPEC run
for i in `seq ${COUNT}`
do
	echo "Launching instance - ${i}"
	runspec --config=${CONFIG} --nobuild --noreportable all_cpp &
done

