#!/bin/bash

# Load Global variables from file
. 00_variables.sh

# On a 64-bit system make sure the 32-bit binaries are installed
yum install glibc-devel.i686 libstdc++-devel.i686 bc

# Fetch the HEPSPEC tarball
mkdir -p ${DOWNLOAD}

if [ ! -d ${DOWNLOAD} ]; then
	echo "ERROR: could not create download directory"
	exit 1
fi

cd ${DOWNLOAD}
echo "In working directory: ${PWD}"

# curl tarball from some location
# curl linux32-gcc_cern.cfg
# curl linux64-gcc_cern.cfg

if [ ! -e ./${TARBALL} ]; then
	echo "ERROR: unable to download HEPSPEC tarball"
	exit 1
fi

# Untar HEPSPEC
tar jxvf ${TARBALL}


# Install HEPSPEC
cd SPEC_CPU2006v1.1
./install.sh -d ${INSTALL} -f

# Copy in config:
cp ../linux32-gcc_cern.cfg ${INSTALL}/cern32.cfg
cp ../linux64-gcc_cern.cfg ${INSTALL}/cern64.cfg

